# tfs
Trello File System. 
Manage trello like a file system.

![CreateBoard](samples/CreateBoardv2.gif)

## Setup
    
1. Clone the git project
2. Install REST::Client perl package.
   - **yum** `yum install perl-REST-Client`
   - **cpan** `perl -MCPAN -e 'install REST::Client'`

3. Install Fuse perl package.
   - **yum** `yum install perl-Fuse`
   - **cpan** `perl -MCPAN -e 'install Fuse'`

4. Install JSON::XS perl package.
   - **yum** `yum install perl-JSON-XS`
   - **cpan** `perl -MCPAN -e 'install JSON::XS'`
   
5. Go on git project repository

6. Create the file Config.ini from Config.sample.ini.
   - `cp Config.sample.ini Config.ini`
   
7. Configure the Config.ini file.
   - The **key** can be retreive on **https://trello.com/app-key**.
   - The **token** can be retreive on the same website as the **key**
     by clicking on the token hyperlink.
   - For more information on the authentification to trello platform
     go to **https://developers.trello.com/docs/api-introduction**.
   - The **organisation** is the id of your trello team, to get this
     id go on **https://trello.com** and select the good team. when
     you have select the team the url will change in
     **https://trello.com/teamid/home**.
   - The **timeBeforeRefresh** prevent tfs to send the same request
     too often.
   - The **mountpoint** set the path of mountpoint. The 
     mountpoint need to exist and be empty.

8. Set the PERL5LIB (optional).
    `echo 'export PERL5LIB=/path/to/tfs/lib' >> ~/bashrc`

## Usage
### Launch fuse with PERL5LIB set
    perl tfs.pl

### Launch fuse without PERL5LIB set
    perl -I/path/to/tfs/lib tfs.pl

### Launch fuse with mountpoint in arguments
    perl tfs.pl /path/to/my/mountpoint

## How to
All the path on this section respect the same rules.

1. The path begin all the time by **/path/to/mountpoint**. for example
   the **/** is **/path/to/mountpoint/**.

2. When something is **preceded by #**. this object represente is
       **dynamic** and represent the **name of trello object**.

### List the trello team
    ls /
### List all boards of one team
    ls /#Organisation
### List user of one trello team
    ls /#Organisation/Users/
### List all the card of one board
    ls /#Organisation/#Tableau/Cards
### List all the card of one list
    ls /#Organisation/#Tableau/CardsByList/#List/
### List all the card of one label
    ls /#Organisation/#Tableau/CardsByLabel/#Label/
### List all the card of one user
    ls /#Organisation/#Tableau/CardsByUser/#User/
### List all user on one board
    ls /#Organisation/#Tableau/Users/
### List the content of one card
    ls /#Organisation/#Tableau/Cards/#Card/
    ls /#Organisation/#Tableau/CardsByList/#List/#Card/
    ls /#Organisation/#Tableau/CardsByLabel/#Label/#Card/
    ls /#Organisation/#Tableau/CardsByUser/#User/#Card/

### List the members of the card
    ls /#Organisation/#Tableau/Cards/#Card/Users/
### List the label of the card
    ls /#Organisation/#Tableau/Cards/#Card/Label/
### List all the comment of the card
    ls /#Organisation/#Tableau/Cards/#Card/Commentaire/
### List the content of checklist
    ls /#Organisation/#Tableau/Cards/#Card/#Checklist/
### Display the full json of the card
    cat /#Organisation/#Tableau/Cards/#Card/info
### Display only the description
    cat /#Organisation/#Tableau/Cards/#Card/Description
### Display the end date of the card
    cat /#Organisation/#Tableau/Cards/#Card/DateLimit
### Display full json of one label
    cat /#Organisation/#Tableau/Cards/#Card/Label/#Label
### Display full json of one user
    cat /#Organisation/#Tableau/Cards/#Card/User/#User
### Display the content of one commentary
    cat /#Organisation/#Tableau/Cards/#Card/Commentaire/#Commentaire
### Move the card from one list the an other list.
    mv /#Organisation/#Tableau/CardsByList/#List/#Card /#Organisation/#Tableau/CardsByList/#List2
### Change endDate
    echo null > /#Organisation/#Tableau/Cards/#Card/DateLimit

    echo "2010-10-28T13:00:00" > /#Organisation/#Tableau/Cards/#Card/DateLimit
### Add members to one card (User must exist in the board)
    touch /#Organisation/#Tableau/Cards/#Card/Users/#UserName
### Add label to one card (Label must exist in the board)
    touch /#Organisation/#Tableau/Cards/#Card/Label/#ColorLabel_NameLabel
### Add checklist
    mkdir /#Organisation/#Tableau/Cards/#Card/#ChecklistName
### Add checkitem on checklist
    touch /#Organisation/#Tableau/Cards/#Cards/#Checklist/#CheckItemName
### Add commentary
    touch /#Organisation/#Tableau/Cards/#Card/Commentaire/Commentaire1
### Modify commentary
    echo "hello" > /#Organisation/#Tableau/Cards/#Card/Commentaire/Commentaire1

## Contact
  - Astraud Flavien  *fastraud@integra.fr*
  - Guillemot Alexandre *aguillemot@integra.fr*
  - Rozé Jean-Philippe *jproze@integra.fr*
