# Changelog tfs
All notable changes to this project will be documented in this file.

## Unreleased

## [1.0.0] - 2019-11-06
### Added
Basic action on file system.
*  List the trello team
*  List all boards of one team
*  List user of one trello team
*  List all the card of one board
*  List all the card of one list
*  List all the card of one label
*  List all the card of one user
*  List all user on one board
*  List the content of one card
*  List the members of the card
*  List the label of the card
*  List all the comment of the card
*  List the content of checklist
*  Display the full json of the card
*  Display only the description
*  Display the end date of the card
*  Display full json of one label
*  Display full json of one user
*  Display the content of one commentary
*  Move the card from one list the an other list
*  Change endDate
*  Add members to one card (User must exist in the board)
*  Add label to one card (Label must exist in the board)
*  Add checklist
*  Add checkitem on checklist
*  Add commentary
*  Modify commentary

Configuration file with key
*  key
*  token
*  organisation
*  timeBeforeRefresh
*  mountpoint

