# Authors

## Astraud Flavien <fastraud@itsgroup.fr>
oversight and validation.

## Guillemot Alexandre <aguillemot@integra.fr>
Display of organisation, board, commentary, user ect...
add label, member to card.

## Plisson Luc <lplisson@integra.fr>
Mirroring and CI/CD.

## Ramassamy Caroline <cramassamy@integra.fr>
Display of the card.

