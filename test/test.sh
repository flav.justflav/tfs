#!/usr/bin/sh

# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.





testReturnCode ()
{
	if [ $? -eq 0 ];then
		echo "$1 : OK"
	else
   		echo "$1 Failed"
   		exit  $?
	fi
	
}
Mountpoint=$1
Organisation=$2
Board=$3
List=$4
Label=$5
User=$6
Card=$7
Checklist=""
newUser=$8
i=1
ls $Mountpoint > /dev/null
testReturnCode "Test $i : mountpoint"
i=$((i+1))
ls $Mountpoint/$Organisation > /dev/null
testReturnCode "Test $i : Organisation"
i=$((i+1))
ls $Mountpoint/$Organisation/Users/ > /dev/null
testReturnCode "Test $i : Organisation's user"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board > /dev/null
testReturnCode "Test $i : Board"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/Users > /dev/null
testReturnCode "Test $i : Board's user"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/Cards > /dev/null
testReturnCode "Test $i : get Cards view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/Cards/$Card > /dev/null
testReturnCode "Test $i : get card $Card by Cards view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/Cards/$Card/Users > /dev/null
testReturnCode "Test $i : get card's User by Cards view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/Cards/$Card/Label > /dev/null
testReturnCode "Test $i : get card's Label by Cards view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/Cards/$Card/Commentaire > /dev/null
testReturnCode "Test $i : get card's Commentary by Cards view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/Cards/$Card/$Checklist > /dev/null
testReturnCode "Test $i : get card's $Checklist by Cards view"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/Cards/$Card/Users/$User > /dev/null
testReturnCode "Test $i : display User from card user repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/Cards/$Card/Label/$Label > /dev/null
testReturnCode "Test $i : display label from card label repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/Cards/$Card/Description > /dev/null
testReturnCode "Test $i : display description from card repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/Cards/$Card/info > /dev/null
testReturnCode "Test $i : display information from card repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/Cards/$Card/DateLimit > /dev/null
testReturnCode "Test $i : display description from card repository"
i=$((i+1))




ls $Mountpoint/$Organisation/$Board/CardByUser/ > /dev/null
testReturnCode "Test $i : get CardByUser view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByUser/$User > /dev/null
testReturnCode "Test $i : get all user's card"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card > /dev/null
testReturnCode "Test $i : get one card's User by CardByUser view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/Users > /dev/null
testReturnCode "Test $i : get card's User by CardByUser view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/Label > /dev/null
testReturnCode "Test $i : get card's Label by CardByUser view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/Commentaire > /dev/null
testReturnCode "Test $i : get card's Commentary by CardByUser view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/$Checklist > /dev/null
testReturnCode "Test $i : get card's $Checklist by CardByUser view"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/Users/$User > /dev/null
testReturnCode "Test $i : display User from card user repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/Label/$Label > /dev/null
testReturnCode "Test $i : display label from card label repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/Description > /dev/null
testReturnCode "Test $i : display description from card repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/info > /dev/null
testReturnCode "Test $i : display information from card repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByUser/$User/$Card/DateLimit > /dev/null
testReturnCode "Test $i : display description from card repository"
i=$((i+1))



ls $Mountpoint/$Organisation/$Board/CardByList/ > /dev/null
testReturnCode "Test $i : get CardByList view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByList/$List > /dev/null
testReturnCode "Test $i : get all list's card"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByList/$List/$Card > /dev/null
testReturnCode "Test $i : get one card by CardBylist view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/Users > /dev/null
testReturnCode "Test $i : get card's User by CardByList view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/Label > /dev/null
testReturnCode "Test $i : get card's Label by CardByList view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/Commentaire > /dev/null
testReturnCode "Test $i : get card's Commentary by CardByList view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/$Checklist > /dev/null
testReturnCode "Test $i : get card's $Checklist by CardByList view"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/Users/$User > /dev/null
testReturnCode "Test $i : display User from card user repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/Label/$Label > /dev/null
testReturnCode "Test $i : display label from card label repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/Description > /dev/null
testReturnCode "Test $i : display description from card repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/info > /dev/null
testReturnCode "Test $i : display information from card repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByList/$List/$Card/DateLimit > /dev/null
testReturnCode "Test $i : display description from card repository"
i=$((i+1))

ls $Mountpoint/$Organisation/$Board/CardByLabel/ > /dev/null
testReturnCode "Test $i : get CardByLabel view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByLabel/$Label > /dev/null
testReturnCode "Test $i : get all label's card"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card > /dev/null
testReturnCode "Test $i : get one card by CardBylabel view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Users > /dev/null
testReturnCode "Test $i : get card's User by CardByLabel view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Label > /dev/null
testReturnCode "Test $i : get card's Label by CardByLabel view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Commentaire > /dev/null
testReturnCode "Test $i : get card's Commentary by CardByLabel view"
i=$((i+1))
ls $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/$Checklist > /dev/null
testReturnCode "Test $i : get card's $Checklist by CardByLabel view"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Users/$User > /dev/null
testReturnCode "Test $i : display User from card user repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Label/$Label > /dev/null
testReturnCode "Test $i : display label from card label repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Description > /dev/null
testReturnCode "Test $i : display description from card repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/info > /dev/null
testReturnCode "Test $i : display information from card repository"
i=$((i+1))
cat $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/DateLimit > /dev/null
testReturnCode "Test $i : display description from card repository"
i=$((i+1))

mv $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Users/$User  $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Users/$newUser
sleep 10;
ls $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Users/
ls $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Users/$newUser
mv $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Users/$newUser  $Mountpoint/$Organisation/$Board/CardByLabel/$Label/$Card/Users/$User

echo "Test $i : Try to change members on card"
i=$((i+1))



