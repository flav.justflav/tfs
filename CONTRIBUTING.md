# How to contribute
First off, thanks for taking time to contribute!

I'm really glad you're reading this, because we need help.

## Testing & report bug
Bugs are tracked as [GitLab
issue](https://docs.gitlab.com/ee/user/project/issues/).

Explain the problem and include additional details to help maintainers
reproduce the problem:

  * Use a clear and descriptive title for the issue to identify the problem.
  * Describe the exact steps which reproduce the problem
  * Provide specific examples to demonstrate the steps.
  * Describe the behavior you observed after following the steps and point out what exactly is the problem with that behavior.
  * Explain which behavior you expected to see instead and why.
  * Include screenshots and animated GIFs

## Submitting changes
Please send a merge requests ([How to create a merge
request](https://docs.gitlab.com/ee/gitlab-basics/add-merge-request.html))
with a clear list of what you've done. When you send a pull.

Always write a clear log message for your commits. One-line messages
are fine for small changes, but bigger changes should look like this:

	$ git commit -m "A brief summary of the commit
	> 
	> A paragraph describing what changed and its impact."

## Suggesting new features
Please open an issue to suggest us a new feature.
