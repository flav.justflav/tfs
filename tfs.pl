#!/usr/bin/env perl

# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

use strict;
use warnings;
use Getopt::Long;
use Fuse;
use data::list;
use data::node::file;
use data::node::repository;
use ui::list;
use tools::tfsClient;
use Data::Dumper qw/Dumper/;
use POSIX;
use tools::listUtils;
my $production;

GetOptions ("production" => \$production);

my $lastRequest = time();
sub getdir {
	my $file = tools::listUtils->getFile(shift);
	my $tab;
	my $tfsClient =	tools::tfsClient->getInstance();
	my $time = time;
	my $diff = $time - $lastRequest;
	if ($file->{child} && $tools::tfsClient::timebeforerefresh > $diff)
	{	
		$tab = $file->{child};
	}
	else
	{
		$tab = $file->getChild();
		$lastRequest = $time;
	}
	if (!$tab)
	{
		$tab = [];
	}
	return (@{$tab},0);
}

sub getattr {
	my $path = shift;
	$path =~ s/ /_/g;
	my $file = tools::listUtils->getFile($path);
	if (!$file)
	{
		return ( -ENOENT() );
	}

	my $context = Fuse::fuse_get_context();
	my $size  = length ( $file->{content} ) || 0;
	my $mode  = ( $file->{type} << 9 ) + $file->{mode};
	my $uid   = $context->{uid};
	my $gid   = $context->{gid};
	my $atime = my $ctime = my $mtime = $file->{ctime};
	my ( $dev, $ino, $rdev, $blocks, $nlink, $blksize ) =
		( 0, 0, 0, 1, 1, 1024 );
##Renvoie des informations de l'inode
	return (
			$dev,  $ino,   $mode,  $nlink, $uid,     $gid, $rdev,
			$size, $atime, $mtime, $ctime, $blksize, $blocks
	       );
}

sub myOpen
{
	my $path = shift;
	my $file = tools::listUtils->getFile($path);
	my $flag = shift;
	my $info = shift;

	if (!$file)
	{
		return ( -ENOENT() );
	}
	
	if ($file->{type} == 0400)
	{
		return -EISDIR();
	}
	
	@{$info}{'direct_io','nonseekable'} =(1,1);
	my $content = $file->getContent();
	open (my $fh, "<" , \$content);
	
	##Return du code erreur et du fileHandler créé
	return (0,$fh);

}

sub myRead
{
	
	my ($path, $size, $offset, $fileHandle) = @_;
	my $buffer;
	my $status = read($fileHandle,$buffer,$size,$offset);
	if ($status != 0)
	{
		return $buffer."\n";
	}
	return $status;
}

sub myRename
{
	my $oldFilename = shift;
	my $newFilename = shift;
	my $node = tools::listUtils->getFile($oldFilename);
	$node->move($newFilename);
	
	return 0;
}

sub myMknod
{
	my ($filename, $mode, $device) = @_;
	my $path = "";
	my $name = "";
	if ($filename =~ /(.+)\/(.*)/) 
	{
		$path = $1;
		$name = $2;
	}
	
	my $parent = tools::listUtils->getFile($path);
	$parent->createChild($name);
	return 0;
}

sub myUtime
{
	my $file = tools::listUtils->getFile(shift);
	$file->{ctime} = time();
	$file->{mtime} = time();
	return 0;
}

sub myWrite
{
	my ($path, $buffer, $offset, $fh) = @_;
	my $file = tools::listUtils->getFile($path);
	if ($offset == 0)
	{
		$file->{content} = $buffer;
	}
	else
	{
		$file->{content} = substr($file->{content},0,$offset).$buffer;
	}
	$file->modifyContent();
	return length($buffer);
}

sub myFlush
{
	my ($path, $fh) = @_;
	my $old_fh = select($fh);
	$| = 1;
	select($old_fh);
	return 0;
}

sub myTruncate
{
	return 0;
}

sub myMkDir
{
	my ($filename, $mode, $device) = @_;
	my $path = "";
	my $name = "";
	if ($filename =~ /(.+)\/(.*)/) 
	{
		$path = $1;
		$name = $2;
	}
	
	my $parent = tools::listUtils->getFile($path);
	$parent->createChild($name);
	return 0;

}

sub myUnlink
{
	my $path = shift;
	my $file = tools::listUtils->getFile($path);	
	if ($file->remove() == 0)
	{
		tools::listUtils->remove($path);
	}
	tools::listUtils->getFile($file->{racine})->getChild();
	return 0;
}
sub myRmdir
{
	my $path = shift;
	my $file = tools::listUtils->getFile($path);	
	if ($file->remove() == 0)
	{
		tools::listUtils->remove($path);
	}
	tools::listUtils->getFile($file->{racine})->getChild();
	return 0;

}

my $tfsClient = tools::tfsClient->getInstance("Config.ini"); 

tools::listUtils->init($production);
my $mountpoint = shift @ARGV;
if (!$mountpoint)
{
	$mountpoint = $tfsClient->getKey("mountpoint");
}
Fuse::main(
		mountpoint => $mountpoint,
		getdir => \&getdir,
		getattr => \&getattr,
		open    => \&myOpen,
    		read    => \&myRead,
		rename  => \&myRename,
		mknod   => \&myMknod,
		utime   => \&myUtime,
		write   => \&myWrite,
		unlink  => \&myUnlink,
		flush   => \&myFlush,
		rmdir   => \&myRmdir,
    		truncate    => \&myTruncate,
		mkdir  => \&myMkDir,
		debug => 1,
		threaded => 0,
	  );

