
# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package tools::tfsClient;

=pod

=head1 NAME

tools::tfsClient Manage the REST::Client package
=head1 SYNOPSIS

	use tools::tfsClient
	my $client = tools::tfsClient->getInstance();
	$client->{client}->GET
			   POST
			   ...
	my $hashResponse = $client->getResponse();

	##Encode value
	tools::tfsClient->encodeText($text);

=head1 DESCRIPTION

=head1 FUNCTIONS
=cut

use strict;
use warnings;
use utf8;
use REST::Client;
use JSON::XS;
use tools::parser;
use Data::Dumper qw/Dumper/;

=pod

=head2 _new

Don't use it, use getInstance().
this function initialiser the two package's variable tools::tfsClient::token and tools::tfsClient::key.
It initialise the host of REST::Client to https://api.trello.com/1/

=cut
sub _new
{
	my $class = shift;
	my $file = shift;
	my $config = tools::parser->new($file);

	$config->parseINI();
	
	$tools::tfsClient::token = $config->{"fuseFS"}->{"token"};
	die "Token missing in Config.ini" if (! $tools::tfsClient::token);

	$tools::tfsClient::key = $config->{"fuseFS"}->{"key"};
	die "Key missing in Config.ini" if (! $tools::tfsClient::key);
	
	$tools::tfsClient::organisation = $config->{"fuseFS"}->{"organisation"};
	die "Organisation missing in Config.ini"
	    if (! $tools::tfsClient::organisation);

	$tools::tfsClient::timebeforerefresh =
	    $config->{"fuseFS"}->{"timeBeforeRefresh"};
	$tools::tfsClient::timebeforerefresh = 2
	    if (! $tools::tfsClient::timebeforerefresh);
	
	my $hash = {client => REST::Client->new(), config => $config};
	$hash->{client}->setHost('https://api.trello.com/1/');

	$hash->{client}->GET("members/me/boards?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	if ($hash->{client}->responseCode() != 200)
	{
		die $hash->{client}->responseContent();
	}

	bless $hash, $class;
	return $hash;
		
}


=head2 getKey

This function get the key from the config, you can only get a key is section fuseFS.

=cut

sub getKey
{
	my $self = shift;
	my $key = shift;
	return $self->{config}->{fuseFS}->{$key};
}

=pod

=head2 getInstance

Use the function _new() if no object was created before else return the object already create.

=cut



sub getInstance {
	my $class = shift;
	my $file  = shift;
	if (!$tools::tfsClient::_instance)
	{
		$tools::tfsClient::_instance = tools::tfsClient->_new($file);
	}
	return $tools::tfsClient::_instance;
}

=pod

=head2 getResponse

Test the response code of the client response and if the response code is equals to 200 return the response content json string in hash. if the code is not equals to 200 return undef and print the response content in log.

=cut

sub getResponse {
	my $self = shift;
	if ($self->{client}->responseCode() == 200)
	{
		return JSON::XS->new->utf8->decode($self->{client}->responseContent());
	}
	else
	{
		print Dumper $self->{client}->responseContent();
		my $error = JSON::XS->new->utf8->decode($self->{client}->responseContent());
		if ($error eq "RATE_LIMIT_EXCEEDED")
		{
			print "Too Many Request please wait 10 seconde\n";
			sleep(10);
		}
	}
	print $self->{client}->responseContent(), "\n";
	
	return {};
}


=pod

=head2 encodeText

Encode the text pass in param and change all \s by _ and remove special caractere.

=cut

sub encodeText{
	my $self = shift;
	my $text = shift;
	utf8::encode($text);
	$text =~ s/\s/_/g;
	$text =~ s/\///g;
	#	$text =~ s/["'@\^\-\$]//g;
	$text =~ s/\'//g;
	$text =~ s/\-//g;
	return $text;
}

=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.
Astraud Flavien <fastraud@integra.fr>

=cut


1;
