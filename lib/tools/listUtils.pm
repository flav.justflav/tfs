# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package tools::listUtils;



=pod

=head1 NAME

tools::listUtils Give some function for manage data::list et ui::list

=head1 SYNOPSIS


	use tools::listUtils;

Init list for fuse.
	tools::listUtils->init();

Add a node to the list
	tools::listUtils->addNode( $NODE );

Get a node from the list
	tools::listUtils->getFile( $path );

=head1 DESCRIPTION

This package give you a set of function for manage the list use by fuse.
Actually you can add and get a file.

=head1 FUNCTIONS

=cut


use data::list;
use ui::list;
use data::node::repository;
use Data::Dumper qw/Dumper/;

=pod

=head2 init

This function is used by fuse.pl and it initialise the two list. After the initialisation this function create a node call . who have '/' in path.

=cut

sub init
{


	my $class = shift;
	my $prod = shift;
	my $pathHash = ui::list->getInstance();
	my $dataHash = data::list->getInstance();
	my $file = data::node::repository->new(0755,".","/","/","/");
	$dataHash->addData($file);
	$pathHash->addPath($file);
	$file->getChild();
	if ($prod)
	{
		tools::listUtils->generateTree($file);
	}
}


sub generateTree
{
	my $self = shift;
	my $file = shift;
	print "Parent : $file->{name}\n";
	if ($file->{type} == 0100)
	{
		return 0;
	}
	my $list = $file->getChild();
	
	foreach my $child (@{$list})
	{
		my $l =  ui::list->getInstance();
#		print Dumper $l;
#		print "Enfant : $child\n";
#		print "Path : $file->{path}/$child\n";
		if ($file->{path} eq '/')
		{
			tools::listUtils->generateTree(tools::listUtils->getFile("$file->{path}$child"));
		}
		else
		{
			tools::listUtils->generateTree(tools::listUtils->getFile("$file->{path}/$child"));
		}
	}
	
}
=pod

=head2 getFile

This function take an unix path and return the good file. It use getNodeId and getNodeInfo of the package ui::list and data:::list

=cut

sub getFile
{
	my $class = shift;
	my $path = shift;
	print "-----------------------------------\n File request : $path\n-----------------------------------------\n";
	my $pathHash = ui::list->getInstance();
	my $dataHash = data::list->getInstance();
	my $file = $dataHash->getNodeInfo($pathHash->getNodeId($path));
	if ($file)
	{
		return $file;
	}
	else
	{
		#while ($path ne /\//)
		#{
		#	push @list, $path;
		my $newpath = $path;
		$newpath =~ s/(.+)\/.*/$1/;

		print "-----------------------------------\nNew File request : $newpath\n-----------------------------------------\n";
		#	if ($path !~  /(.+)\/.*/)
		#	{
		#		push @list, $path;
		#		last;
		#	}
		#}

		#@list = reverse @list;
		#foreach my $filePath (@list)
		#{
		my $f = $dataHash->getNodeInfo($pathHash->getNodeId($newpath));
		if ($f)
		{
			$f->getChild();
			my $filer = $dataHash->getNodeInfo($pathHash->getNodeId($path));
			if ($filer)
			{
				return $filer;
			}

		}
		#	else
		#	{
		#		last;
		#	}
		#}

		return undef;
	}
}

=pod

=head2 addNode

This function add a node to both list. It take two node a parent and the child who will be append to list.

=cut

sub addNode
{
	my $class = shift;
	my $pnode = shift;
	my $node =  shift;
	$pnode->appendChild($node);
	data::list->getInstance()->addData($node);
	ui::list->getInstance()->addPath($node);

}

=head2 remove

The function remove call the ui::list->remove function and delete the id object in ui::list instance.
=cut

sub remove
{
	my $class = shift;
	my $path = shift;
	ui::list->getInstance()->remove($path);
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;

