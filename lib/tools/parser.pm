# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


package tools::parser;

=pod

=head1 NAME

tools::parser extract data from the ini config file and stock them.
This don't need to be use by the fuse main because it is already use in the tools::tfsClient package.

=head1 SYNOPSIS


	use tools::parser;

Extract data from ini File.
	$config = tools::parser->new{iniFile};
	$config->parseINI();

getDate from object
	$config->{"fuseFS"}->{"token"};
This exemple take the tolen value in the fuseFS section of the INI file :
[fuseFS]
token=value

=head1 DESCRIPTION

This package get date from the ini file and keep it in memory.

=head1 FUNCTIONS

=cut


use strict;
use warnings;
use JSON::XS;
use Data::Dumper qw/Dumper/;

=head2 new

This function initialise the object, with all the file pass in params.

=cut

sub new
{
	my $class = shift;
	my @files = @_;
	my $parse = {_files => \@files};
	bless $parse, $class;
	return $parse;
}

=head2 parseINI

This function parse data on all files pass to the object constructor.
This function will product new attribute to object.

	$self => {
		_file = [file1.ini, file2.ini],
		SectionName => {
			KeyName => keyValue,
			KeyName2 => keyValue2
		}
	} 

=cut

sub parseINI
{
	my $self = shift;
	my $line;
	my $section;
	foreach my $file (@{$self->{_files}})
	{
		open (FILE, "<:encoding(UTF-8)", $file) or
		    die "can't open file $file : $!";
		while ($line = <FILE>)
		{
			next if $line =~ /^#/;
			if ($line  =~ /^\[([\w=]+)\]/) {
				$section = $1;
				if (not exists $self->{$section}) {
					$self->{$section} = {};
				}

			} elsif ($line =~ /([\w\-\.]+)\s*=\s*(.+)?$/) {
				my ($key, $val) = ($1,$2);
				$self->{$section}->{$key} = $val;
			}
		}
	}
}

=head2 addSection

add a new section to the config object.

=cut

sub addSection
{
	my $self = shift;
	my $section = shift;
	$self->{$section} = undef;
}

=head2 addKey

add a new key/value to the config object on the section pass in params

=cut

sub addKey
{
	my $self = shift;
	my $section = shift;
	my $key = shift;
	my $value = shift;
	$self->{$section}->{$key} = $value;
}
1;

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


