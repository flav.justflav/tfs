# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.



package ui::list;

use strict;
use warnings;

=pod

=head1 NAME

ui::list List of path to acces data::list

=head1 SYNOPSIS

	use ui::list;
	ui::list->getInstance();


=head1 DESCRIPTION

This package keep all path of node for fuseFS, all this path refere to on data::list id. multiple path can refered to the same data::list id

=head1 FUNCTIONS

=cut



=pod

=head2 _new

This is a private constructor for the package don't use it. This function s already use by the function getInstance

=cut

sub _new {
	my $class = shift;
	my $hash = {};
	bless $hash, $class;
	return $hash;
}

=pod

=head2 getInstance

This function initialise the list and return it.

=cut


sub getInstance {
	if (!$ui::list::_instance)
	{
		$ui::list::_instance = ui::list->_new();
	}
	return $ui::list::_instance;
}

=pod

=head2 getNodeId

Return the data::list id with the path pass in param
in : path
out : id of data::list object



=cut

sub getNodeId{
	my $self = shift;
	my $datapath = shift;
	return $self->{$datapath};
}

=pod

=head2 addPath

Add object id to the list, take the argument path to refere the data::list id who is represent by arguemnt id of hash.
in : hash
out : void

=cut

sub addPath {

	my $self = shift;
	my $hash = shift;
	$self->{$hash->{path}} = $hash->{id};
}

=head2 remove
	
Remove the path toward the object id.
 
=cut

sub remove {
	my $self = shift;
	my $path = shift;
	$self->{$path} = undef;
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut

1;
