# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository;
use parent "data::node";
use strict;
use warnings;
use tools::tfsClient;
use data::node::repository::organisation;
=pod

=head1 NAME

data::node::repository

=head1 SYNOPSIS

	use data::node::repository;

	data::node::repository->new(MODE, NAME, RACINE, ID, PATH);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository. This object has a new attribute call child.

=head1 FUNCTIONS
	
=cut

=pod

=head2 new

This is the constructor of the data::node::repository package, he take a UNIX mode , a name , a racine , an id and a path in params and return the object.
IN : Unix mode, String name , String Racine , String Id, String Path
OUT : data::node::repository

=head3 data::node::repository object

	data::node::repository :	
	{
		mode   => Unix mode,
		name   => String,
		id     => String,
		racine => String,
		path   => String,
		uid    => Uid number of current user,
		ctime  => Time of the node creation,
		mtime  => Time of the node creation,
		gid    => Array of gid,
		type   => 0100,
		child  => Array of id,

	}

=cut

sub new {
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = shift;

	if (!$path)
	{
		$path = "$racine/$name";
	}

	my $node = $class->SUPER::new($mode,$name,$racine,$id,$path);
	$node->{type} = 0040;
	$node->{child} = ();
	bless $node, $class;
	return $node;
}

=pod

=head2 getChild

Return the child of the node. this function get the data from a trello organization (team).

=cut

sub getChild
{
	my $self = shift;	
	$self->{child} = ();
	my $tfsClient =	tools::tfsClient->getInstance();

	$tfsClient->{client}->GET("organizations/$tools::tfsClient::organisation?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $orga = $tfsClient->getResponse();
	if ($orga)
	{
		$orga->{name} = tools::tfsClient->encodeText($orga->{name});	
		my $node = data::node::repository::organisation->new(0755, $orga->{name}, $self->{path}, $orga->{id});
		$node->{TrelloId} = $orga->{id};
		$self->appendChild($node);
		data::list->getInstance()->addData($node);	
		ui::list->getInstance()->addPath($node);
	}		
	return $self->{child};
}

=pod

=head2 appendChild

Add the node name to the child attribute of the node.

=cut

sub appendChild
{
	my $self = shift;
	my $node = shift;
	push @{$self->{child}}, $node->{name};
}

=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
