# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::file;
use parent "data::node";
use strict;
use warnings;

=pod

=head1 NAME

data::node::file

=head1 SYNOPSIS

	use data::node::file;

	data::node::file->new(MODE, NAME, RACINE, ID, PATH, CONTENT);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type file. This object has the content attribute.

=head1 FUNCTIONS
	
=cut

=pod

=head2 new

This is the constructor of the data::node::file package, he take a UNIX mode , a name , a racine , an id,  a path and a content in params and return the object.
IN : Unix mode, String name , String Racine , String Id, String Path, String content
OUT : data::node::file

=head3 data::node::file object

	data::node::file :	
	{
		mode   => Unix mode,
		name   => String,
		id     => String,
		racine => String,
		path   => String,
		uid    => Uid number of current user,
		ctime  => Time of the node creation,
		mtime  => Time of the node creation,
		gid    => Array of gid,
		type   => 0100,
		content => String,

	}

=cut


sub new {
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = shift;
	my $content = shift;


	my $node = $class->SUPER::new($mode,$name,$racine,$id,$path);
	$node->{type} = 0100;
	$node->{content} = $content;
	bless $node, $class;
	return $node;
}


=pod

=head2 getContent

Get Accessor for the content attribute.

=cut


sub getContent
{	
	my $self = shift;
	return $self->{content};
}


=pod

=head2 setContent

Set Accessor for the content attribute.

=cut


sub setContent
{
	my $self = shift;
	my $content = shift;

	$self->{content} = $content;
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
