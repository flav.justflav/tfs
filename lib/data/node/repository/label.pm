# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::label;

use parent "data::node::repository";
use strict;
use warnings;
use utf8;
use URI::Escape;
use data::node::repository::card; 
=pod

=head1 NAME

data::node::repository::label

=head1 SYNOPSIS
	use data::node::repository::label

	data::node::repository::label->new(MODE, NAME, RACINE, ID, PATH, REALNAME);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository and call label. It override the getChild function who give all the card for one label.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::label like data::node::repository he take Unix mode, a name, a racine, an id, a path and his realName. Return the object.


=cut



sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = shift;
	my $realName = shift;
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	utf8::encode($realName);
	$node->{realName} = $realName;

	bless $node, $class;
	return $node;
}

=pod

=head2 getChild

Return the childs of the node. this function get all the card tag with the label and create a data::node::repository::card object with each of them.

=cut

sub getChild
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();

	$self->{child} = [];
	my $parent = tools::listUtils->getFile($self->{'racine'});
	my $board = tools::listUtils->getFile($parent->{'racine'});
	my $uriEscapeName = "";
	if ($self->{realName} eq '')
	{
		my $string = $self->{name};
		$string =~ s/_$//;
		$uriEscapeName = uri_escape($string);
	}
	else
	{
		$uriEscapeName = uri_escape($self->{realName});
	}
	$tfsClient->{client}->GET("search?query=label%3A%22$uriEscapeName%22&idBoards=$board->{TrelloId}&modelTypes=all&board_fields=name%2CidOrganization&boards_limit=10&card_fields=all&cards_limit=1000&cards_page=0&card_board=false&card_list=false&card_members=false&card_stickers=false&card_attachments=false&organization_fields=name%2CdisplayName&organizations_limit=10&member_fields=avatarHash%2CfullName%2Cinitials%2Cusername%2Cconfirmed&members_limit=10&partial=false&key=$tools::tfsClient::key&token=$tools::tfsClient::token");

	my $search = $tfsClient->getResponse();
	foreach my $c (@{$search->{cards}})
	{
		$c->{name} = tools::tfsClient->encodeText($c->{name});	
		my $node = data::node::repository::card->new(0755, $c->{'name'}, $self->{'path'}, $c->{'id'}, "$self->{path}/$c->{name}");
		$node->{TrelloId} = $c->{'id'};
		tools::listUtils->addNode($self, $node);
	}

	return $self->{child};  
}

=head2 remove

This function remove label from trello board.

=cut

sub remove
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();

	$tfsClient->{client}->DELETE("labels/$self->{TrelloId}?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	$tfsClient->getResponse();
}

=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut



1;
