# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board::customfields;

use parent "data::node::repository";
use strict;
use utf8;
use warnings;
use data::node::file::customfields;
use Data::Dumper qw/Dumper/;

sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}

sub getChild{
	my $self = shift;
	
	$self->{'child'} = [];
	my $parent = tools::listUtils->getFile($self->{'racine'});
	my $list = data::list::customfields->getInstance();

	print Dumper $parent;
	print $self->{TrelloId};

	foreach my $id (keys %{$list->{$parent->{TrelloId}}}) {
	    my $node =
		data::node::file::customfields->new(0755,
						   $list->{$parent->{TrelloId}}->{$id}->{'name'},
						   $self->{'path'},
						   $list->{$parent->{TrelloId}}->{$id}->{id},
						   "$self->{'path'}/$list->{$parent->{TrelloId}}->{$id}->{'name'}", $list->{$parent->{TrelloId}}->{$id});
	    tools::listUtils->addNode($self, $node);
	}
	return $self->{'child'};
}

1;
