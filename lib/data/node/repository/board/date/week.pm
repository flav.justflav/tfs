# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board::date::week;

use parent "data::node::repository";
use strict;
use warnings;
use utf8;
use URI::Escape;
use data::node::repository::card; 

=pod

=head1 NAME

data::node::repository::board::date::week

=head1 SYNOPSIS
	use data::node::repository::board::date::week

	data::node::repository::board::date::week->new(MODE, NAME, RACINE, ID);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository . It override the getChild function which contain card when their due date is less or equals to 1 week.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::board::date::day like data::node::repository he take Unix mode, a name, a racine, an id, a path and his realName. Return the object.


=cut



sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = shift;
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);


	bless $node, $class;
	return $node;
}

=head2 getChild

This function get all cards where her due date is equals or less than 1 week.

=cut


sub getChild
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();

	$self->{child} = [];
	my $parent = tools::listUtils->getFile($self->{'racine'});
	my $board = tools::listUtils->getFile($parent->{'racine'});

	while (ref $board ne 'data::node::repository::board' || ref $board eq 'data::node::repository')
	{
		$board = tools::listUtils->getFile($board->{'racine'});
	}

	$tfsClient->{client}->GET("search?query=due%3Aweek&idBoards=$board->{TrelloId}&modelTypes=all&board_fields=name%2CidOrganization&boards_limit=10&card_fields=all&cards_limit=1000&cards_page=0&card_board=false&card_list=false&card_members=false&card_stickers=false&card_attachments=false&organization_fields=name%2CdisplayName&organizations_limit=10&member_fields=avatarHash%2CfullName%2Cinitials%2Cusername%2Cconfirmed&members_limit=10&partial=false&key=$tools::tfsClient::key&token=$tools::tfsClient::token");

	my $search = $tfsClient->getResponse();
	foreach my $c (@{$search->{cards}})
	{
		$c->{name} = tools::tfsClient->encodeText($c->{name});	
		my $node = data::node::repository::card->new(0755, $c->{'name'}, $self->{'path'}, $c->{'id'}, "$self->{path}/$c->{name}");
		$node->{TrelloId} = $c->{'id'};
		tools::listUtils->addNode($self, $node);
	}

	return $self->{child};  
}

1;

=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut
