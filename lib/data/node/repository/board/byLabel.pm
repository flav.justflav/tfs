# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board::byLabel;

use parent "data::node::repository";
use strict;
use warnings;
use utf8;

use tools::tfsClient;
use tools::listUtils;

use Data::Dumper qw/Dumper/;
use data::node::repository::label;
use data::list::label;

=pod

=head1 NAME

data::node::repository::board::byLabel

=head1 SYNOPSIS
	use data::node::repository::board::byLabel

	data::node::repository::board::byLabel->new(MODE, NAME, RACINE, ID, PATH);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository. It override the getChild function and get all label of the board.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::board::byLabel like data::node::repository he take Unix mode, a name, a racine and an id and return the object.

=cut

sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine  = shift;
	my $id = shift;
	my $path = shift;

	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);
	
	bless $node, $class;
	return $node;
}

=head2 getChild

Return the child of the node. this function get all the label of one boards and create a data::node::repository::label with each of them.

=cut


sub getChild{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	my $node;

	$self->{child} = [];
	my $parent = tools::listUtils->getFile($self->{'racine'});

	$tfsClient->{client}->GET("boards/$parent->{TrelloId}/labels?key=$tools::tfsClient::key&token=$tools::tfsClient::token");

	my $labels = $tfsClient->getResponse();
	
	foreach my $val (@{$labels}){
		my $realName = $val->{name};
		$val->{name} = tools::tfsClient->encodeText($val->{name});	
		$node = data::node::repository::label->new(0755, "$val->{color}_$val->{name}", $self->{'path'}, $val->{'id'}, "$self->{path}/$val->{color}_$val->{name}",$realName);
		$node->{TrelloId} = $val->{id};
		tools::listUtils->addNode($self, $node);
		$val->{realName} = $realName;
		data::list::label->getInstance()->addLabel($val,$parent);
	}
	return $self->{'child'};
}
=pod

=head2 createChild

create a new trello label with specific color and name

=cut

sub createChild 
{
	my $self = shift;
	my $name = shift;
	my $color = "";
	($color, $name) = $name =~ /(.+)?_(.+)?/;

	my $parent = tools::listUtils->getFile($self->{racine});	
	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{'client'}->POST("boards/$parent->{id}/labels?name=$name&color=$color&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	$tfsClient->getResponse();
	$self->getChild();
	return 0;


}


=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut
1;
