# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board::byList;

use parent "data::node::repository";
use strict;
use utf8;
use warnings;
use data::node::repository::list;

=pod

=head1 NAME

data::node::repository::board::byList

=head1 SYNOPSIS
	use data::node::repository::board::byList

	data::node::repository::board::byList->new(MODE, NAME, RACINE, ID);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository. It override the getChild function and get all list of the board.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::board::byList like data::node::repository he take Unix mode, a name, a racine and an id and return the object.

=cut


sub new
{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}


=head2 getChild

Return the child of the node. this function get all the list of one boards and create a data::node::repository::list with each of them.

=cut


sub getChild
{
	my $self = shift;
	my $tfsClient =	tools::tfsClient->getInstance();

	$self->{child} = [];

	my $parent = tools::listUtils->getFile($self->{racine});	


	$tfsClient->{client}->GET("boards/$parent->{TrelloId}/lists?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $lists = $tfsClient->getResponse();
	foreach my $l (@{$lists})
	{
		$l->{name} = tools::tfsClient->encodeText($l->{name});	
		my $node = data::node::repository::list->new(0755,$l->{name},$self->{path},$l->{id});
		$node->{TrelloId} = $l->{id};
		tools::listUtils->addNode($self,$node);
	}
	return $self->{child};	
}

=head2 createChild

create a new trello list

=cut

sub createChild 
{
	my $self = shift;
	my $name = shift;
	my $parent = tools::listUtils->getFile($self->{racine});	
	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{'client'}->POST("boards/$parent->{id}/lists?name=$name&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	$tfsClient->getResponse();
	$self->getChild();
	return 0;


}


=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut
1;
