# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board::byDate;

use parent "data::node::repository";
use strict;
use warnings;
use utf8;

use tools::tfsClient;
use tools::listUtils;
use data::node::repository::board::date::day;
use data::node::repository::board::date::week;
use data::node::repository::board::date::month;
use data::node::repository::board::date::over;
use data::node::repository::board::date::complete;
use data::node::repository::board::date::incomplete;

use Data::Dumper qw/Dumper/;

=pod

=head1 NAME

data::node::repository::board::byDate

=head1 SYNOPSIS
	use data::node::repository::board::byDate;

	data::node::repository::board::byDate->new(MODE, NAME, RACINE, ID);

	data::node::repository::board::byDate->getChild();

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository. It override the getChild function. This package create a Date repository.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::board::byDate like data::node::repository he take Unix mode, a name, a racine and an id and return the object.


=cut


sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine  = shift;
	my $id = shift;
	my $path = shift;

	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);
	
	bless $node, $class;
	return $node;
}

=pod 

=head2 getChild

Return the childs of the node. this function call the function getDueMonth, getDueWeek, getDueDay, getDueOber, getDueComplete, getDueIncomplete.

=cut


sub getChild
{
	my $self = shift;
	$self->{child} = [];	
	$self->getDueMonth();
	$self->getDueWeek();
	$self->getDueDay();
	$self->getDueOver();
	$self->getDueComplete();
	$self->getDueIncomplete();
	return $self->{child};
}

=head2 getDueMonth

This function add to the child a data::node::repository::board::date::month object.

=cut

sub getDueMonth
{
	my $self = shift;
	my $node = data::node::repository::board::date::month->new(0755, "Month", $self->{'path'}, "$self->{id}/Month");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);


}
sub getDueDay
{
	my $self = shift;
	my $node = data::node::repository::board::date::day->new(0755, "Day", $self->{'path'}, "$self->{id}/Day");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);


}
sub getDueWeek
{
	my $self = shift;
	my $node = data::node::repository::board::date::week->new(0755, "Week", $self->{'path'}, "$self->{id}/Week");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);


}
sub getDueOver
{
	my $self = shift;
	my $node = data::node::repository::board::date::over->new(0755, "Over", $self->{'path'}, "$self->{id}/Over");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);


}
sub getDueComplete
{
	my $self = shift;
	my $node = data::node::repository::board::date::complete->new(0755, "Complete", $self->{'path'}, "$self->{id}/Complete");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);


}
sub getDueIncomplete
{
	my $self = shift;
	my $node = data::node::repository::board::date::incomplete->new(0755, "Incomplete", $self->{'path'}, "$self->{id}/Incomplete");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);


}
1;

