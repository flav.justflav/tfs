# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board::archive;

use parent "data::node::repository";
use strict;
use utf8;
use warnings;
use tools::listUtils;
use tools::tfsClient;
use data::node::repository::board::archive::list;
use data::node::repository::board::archive::card;
use Data::Dumper qw/Dumper/;

=pod

=head1 NAME

data::node::repository::board::archive

=head1 SYNOPSIS
	use data::node::repository::board::archive;

	data::node::repository::board::archive->new(MODE, NAME, RACINE, ID);

	data::node::repository::board::archive->getChild();
MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository. It override the getChild function. This repository represent all object in the trello board archive.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::board::archive like data::node::repository he take Unix mode, a name, a racine and an id and return the object.


=cut


sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}

=pod 

=head2 getChild

Return the childs of the node. this function call the function getListArchive and getCardArchive.

=cut


sub getChild{
	my $self = shift;
	$self->getListArchive();
	$self->getCardArchive();	
	return $self->{'child'};
}

=pod

=head2 getListArchive

this function add to the child a data::node::repository::board::archive::list object.
 
=cut


sub getListArchive
{
	my $self = shift;	
	my $node = data::node::repository::board::archive::list->new(0755, "List", $self->{'path'}, "$self->{id}/List");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);

}

=pod

=head2 getCardArchive

this function add to the child a data::node::repository::board::archive::list object.
 
=cut


sub getCardArchive
{
	my $self = shift;	
	my $node = data::node::repository::board::archive::card->new(0755, "Card", $self->{'path'}, "$self->{id}/Card");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);
}

1;

=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


