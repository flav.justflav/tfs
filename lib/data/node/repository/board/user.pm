# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board::user;

use parent "data::node::repository";
use strict;
use utf8;
use warnings;
use data::node::file::user;

=pod

=head1 NAME

data::node::repository::board::user

=head1 SYNOPSIS
	use data::node::repository::board::user

	data::node::repository::board::user->new(MODE, NAME, RACINE, ID);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository and call user. It override the getChild function.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::board::user like data::node::repository he take Unix mode, a name, a racine and an id and return the object.


=cut


sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}
=pod 

=head2 getChild

Return the child of the node. this function get all the members of one boards and create a data::node::file::user with each of them.

=cut


sub getChild{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	
	$self->{'child'} = [];
	
	my $parent = tools::listUtils->getFile($self->{'racine'});
	
	$tfsClient->{'client'}->GET("boards/$parent->{TrelloId}/members?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $users = $tfsClient->getResponse();

	foreach my $val (@{$users}){
		$val->{fullName} = tools::tfsClient->encodeText($val->{fullName});	
		my $node = data::node::file::user->new(0755, $val->{'fullName'},$self->{'path'}, "boardU/".$val->{id}, "$self->{'path'}/$val->{'fullName'}", $val);	
		$node->{TrelloId} = $val->{id};
		tools::listUtils->addNode($self,$node);
	}
	return $self->{'child'};
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
