# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board;
use parent "data::node::repository";
use data::node::repository::board::user;
use strict;
use warnings;


sub new{

	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);
	
	bless $node, $class;
	return $node;
}

sub getChild {
	
	my $self = shift;
	
	$self->getUser();
	return $self->{'child'};
}

sub getUser{
	my $self = shift;
	
	my $user = data::node::repository::board::user->new(0755, "Users", $self->{'path'}, $self->{'id'});
	$self->appendChild($node);
	data::list->getInstance()->addData($node);
	ui::list->getInstance()->addPath($node);
}

1;
