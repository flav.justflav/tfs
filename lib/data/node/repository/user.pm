# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::user;

use parent "data::node::repository";
use strict;
use Data::Dumper qw/Dumper/;
use utf8;
use warnings;

=pod

=head1 NAME

data::node::repository::user

=head1 SYNOPSIS
	use data::node::repository::user

	data::node::repository::user->new(MODE, NAME, RACINE, ID);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository and call user. It override the getChild function who give all the card for one user.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::label like data::node::repository he take Unix mode, a name, a racine, an id, a path and his realName. Return the object.


=cut



sub new
{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}

=head2 getChild

This function get all cards of one user and create a data::node::repository::card for each of them.

=cut

sub getChild
{
	my $self = shift;
	my $tfsClient =	tools::tfsClient->getInstance();

	$self->{child} = [];
	
	my $parent = tools::listUtils->getFile($self->{'racine'});
	my $board = tools::listUtils->getFile($parent->{'racine'});
        $tfsClient->{client}->GET("search?query=%40$self->{TrelloId}&idBoards=$board->{TrelloId}&modelTypes=all&boards_limit=10&card_fields=all&cards_limit=10&cards_page=0&card_board=false&card_list=false&card_members=false&card_stickers=false&card_attachments=false&organization_fields=name%2CdisplayName&organizations_limit=10&member_fields=avatarHash%2CfullName%2Cinitials%2Cusername%2Cconfirmed&members_limit=10&partial=false&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $search = $tfsClient->getResponse();
	foreach my $c (@{$search->{cards}})
	{
		$c->{name} = tools::tfsClient->encodeText($c->{name});	
		my $node = data::node::repository::card->new(0755, $c->{'name'}, $self->{'path'}, $c->{'id'}, "$self->{path}/$c->{name}");
		$node->{TrelloId} = $c->{id};
		tools::listUtils->addNode($self, $node);
		                
	}

	return $self->{child};	
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
