# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::card::label;

use parent "data::node::repository";
use strict;
use utf8;
use warnings;
use data::node::file::label;
use Data::Dumper qw/Dumper/;

=pod

=head1 NAME

data::node::repository::card::label

=head1 SYNOPSIS
	use data::node::repository::card::label

	data::node::repository::card::label->new(MODE, NAME, RACINE, ID);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository and call label. It override the getChild function.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::card::label like data::node::repository he take Unix mode, a name, a racine and an id and return the object.


=cut


sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}
=pod 

=head2 getChild

Return the child of the node. this function get all labels of one card and create a data::node::file::user with each of them.

=cut


sub getChild{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	
	$self->{'child'} = [];
	
	my $parent = tools::listUtils->getFile($self->{'racine'});
	$tfsClient->{'client'}->GET("cards/$parent->{TrelloId}?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $card = $tfsClient->getResponse();

	foreach my $label (@{$card->{labels}}){
		my $realName = $label->{name};
		$label->{name} = tools::tfsClient->encodeText($label->{name});	
		my $node = data::node::file::label->new(0755, "$label->{color}_$label->{name}", $self->{'path'}, $label->{'id'}."/$label->{name}", "$self->{path}/$label->{color}_$label->{name}",$realName);
		$node->{TrelloId} = $label->{id};
		tools::listUtils->addNode($self,$node);
	}
	return $self->{'child'};
}

=head2 createChild

This function add a label to the trello card
=cut

sub createChild {
	my $self = shift;
	my $name = shift;
	##Carte
	my $parent = tools::listUtils->getFile($self->{'racine'});
	##vue
	my $pp = tools::listUtils->getFile($parent->{'racine'});
	##Tableau
	my $board = tools::listUtils->getFile($pp->{'racine'});
	if (ref($board) ne "data::node::repository::board")
	{
		$board = tools::listUtils->getFile($board->{racine});
	}
	my $tfsClient = tools::tfsClient->getInstance();
	my $labelList = data::list::label->getInstance();
	my $labelId = "";

	if (($labelId) = $labelList->checkLabelExist($name,$board->{TrelloId}))
	{
		$tfsClient->{'client'}->POST("cards/$parent->{TrelloId}/idLabels?value=$labelId&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
		$tfsClient->getResponse();
		$self->getChild();
		return 0;
	}
	else
	{
		##Que faire
	}
		
	
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1
