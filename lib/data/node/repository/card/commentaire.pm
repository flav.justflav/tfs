# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::card::commentaire;

use parent "data::node::repository";
use strict;
use utf8;
use warnings;
use data::node::file::card::commentaire;
use Data::Dumper qw/Dumper/;

sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}
=pod 

=head2 getChild

Return the child of the node. this function get all checkitem of one commentaire and create a data::node::file::card::commentaire with each of them.

=cut


sub getChild
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	my $parent  = tools::listUtils->getFile($self->{racine});	
	$self->{'child'} = [];
	
#	print Dumper $parent;
	
	$tfsClient->{'client'}->GET("cards/$parent->{TrelloId}/actions?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $commentaire = $tfsClient->getResponse();
	$data::node::repository::card::commentaire::int = 0;
	foreach my $val (@{$commentaire}){
		if ($val->{'type'} eq 'commentCard')
		{
			$val->{name} = "Commentaire$data::node::repository::card::commentaire::int";
			$data::node::repository::card::commentaire::int++;
			$val->{name} = tools::tfsClient->encodeText($val->{name});	
			my $node = data::node::file::card::commentaire->new(0755, $val->{'name'},$self->{'path'}, "$val->{id}", "$self->{'path'}/$val->{'name'}", $val);	
			$node->{TrelloId} = $val->{id};
			tools::listUtils->addNode($self,$node);
		}
	}
	return $self->{'child'};
}

=head2 createChild

This function create a new commentary on trello card.
=cut

sub createChild 
{
	my $self = shift;
	my $name = shift;
	#$name = "Commentaire$data::node::repository::card::commentaire::int";
	my $parent  = tools::listUtils->getFile($self->{racine});	
	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{'client'}->POST("cards/$parent->{TrelloId}/actions/comments?text= &key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $response = $tfsClient->getResponse();
	my $node = data::node::file::card::commentaire->new(0755, $name,$self->{'path'}, "$response->{id}", "$self->{'path'}/$name", "");	
	$node->{TrelloId} = $response->{id};
	tools::listUtils->addNode($self,$node);

#	$self->getChild();
	return 0;


}


=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
