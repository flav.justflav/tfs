# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::card::customFields;

use parent "data::node::repository";
use strict;
use utf8;
use warnings;
use data::node::file::customfields;
use Data::Dumper qw/Dumper/;

sub new{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}

sub getChild{
	my $self = shift;

	$self->{'child'} = [];
	my $parent  = tools::listUtils->getFile($self->{racine});	
	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{client}->GET("/cards/$parent->{TrelloId}/?fields=name&customFields=true&key=$tools::tfsClient::key&token=$tools::tfsClient::token");

	my $h_customfields = $tfsClient->getResponse();
	foreach my $customfield (@{$h_customfields->{customFields}}) {
		$customfield->{name} = tools::tfsClient->encodeText($customfield->{name});
		print Dumper $customfield;
		my $node =
			data::node::file::customfields->new(0755,
					$customfield->{'name'},
					$self->{'path'},
					$customfield->{id},
					"$self->{'path'}/$customfield->{'name'}",
					$customfield->{'text'});
		tools::listUtils->addNode($self, $node);
	}
	return $self->{'child'};
}

1;
