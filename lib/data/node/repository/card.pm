# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::card;
use parent "data::node::repository";
use strict;
use warnings;
use data::node::repository::card::user;
use data::node::repository::card::label;
use data::node::repository::card::checklist;
use data::node::repository::card::commentaire;
use data::node::repository::card::customFields;
use data::node::file::card::description;
use data::node::file::card::dateLimit;
use data::node::file::card::info;
use Data::Dumper qw/Dumper/;

=pod

=head1 NAME

data::node::repository::card

=head1 SYNOPSIS
	use data::node::repository::card

	data::node::repository::card->new(MODE, NAME, RACINE, ID, PATH);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository and call card. It override the getChild function.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::card like data::node::repository he take Unix mode, a name, a racine and an id and return the object.


=cut


sub new{
	my $class = shift;
	my $mode = shift;
	my $name  =  shift;
	my $racine = shift;
	my $id = shift;
	my $path = shift;

	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);
	bless $node, $class;
	return $node;
}

=pod

=head2 getChild

Return the childs of the node. this function call the function getInfo, getDescription, getDateLimit, getLabel, getUser, getCommentaire.

=cut

sub getChild
{
    my $self = shift;
    $self->{child} = [];
    $self->getInfo();
    $self->getDescription();
    $self->getChecklist();
    $self->getDateLimit();
    $self->getUser();
    $self->getLabel();
    $self->getCommentaire();
    $self->getCustomFields();
    return $self->{child};
}

sub getCustomFields
{
	my $self = shift;
	my $node = data::node::repository::card::customFields->new(0755, "CustomFields", $self->{'path'}, "$self->{id}/CustomFields");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);
}

=head2 getChecklist

Get all checklist of one card and create a data::node::repository::card::checklist object for each of them.

=cut
sub getChecklist
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	
	$tfsClient->{'client'}->GET("cards/$self->{TrelloId}/checklists?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $checklist = $tfsClient->getResponse();
	foreach my $checklist (@{$checklist})
	{
		$checklist->{name} = tools::tfsClient->encodeText($checklist->{name});	
		my $node = data::node::repository::card::checklist->new(0755, $checklist->{name},$self->{path},$checklist->{id});
		$node->{TrelloId} = $checklist->{id};
		tools::listUtils->addNode($self,$node);
	}
}

=head2 getInfo

get the full json on one card and create a data::node::file::card::info object.

=cut

sub getInfo
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{'client'}->GET("cards/$self->{'TrelloId'}?fields=all&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $card = $tfsClient->getResponse();
	
	my $node = data::node::file::card::info->new(0755, "info", $self->{'path'}, "$self->{id}/info",$card);
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);
}

=head2 getUser

Create a user repository with the type data::node::repository::card::user

=cut

sub getUser
{
	my $self = shift;
	my $node = data::node::repository::card::user->new(0755, "Users", $self->{'path'}, "$self->{id}/Users");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);
}

=head2 getLabel

Create a label repository with the type data::node::repository::card::label

=cut

sub getLabel
{
	my $self = shift;
	my $node = data::node::repository::card::label->new(0755, "Label", $self->{'path'}, "$self->{id}/Label");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);
}

=head2 move

move the card we have multiple case :

if you move from the archive card repository, you unarchived the card

if you move toward a list this function will change the current list of the card
=cut


sub move
{
	my $self = shift;
	my $newPath = shift;
	my $path = "";
	my $name ="";
	if ($newPath =~ /(.+)\/(.*)/)
	{
		$path = $1;
		$name = $2;
	}
	my $parent = tools::listUtils->getFile($path);
	my $cardParent = tools::listUtils->getFile($self->{racine});

	my $type = ref($parent);
	my $tfsClient = tools::tfsClient->getInstance();
	print Dumper $type;
	if (ref $cardParent eq 'data::node::repository::board::archive::card' && $type eq 'data::node::repository::list')
	{
		$tfsClient->{'client'}->PUT("cards/$self->{'TrelloId'}?name=$name&closed=false&idList=$parent->{'TrelloId'}&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	}
	elsif (ref $cardParent eq 'data::node::repository::board::archive::card' && $type eq 'data::node::repository::board::cards')
	{
		$tfsClient->{'client'}->PUT("cards/$self->{'TrelloId'}?name=$name&closed=false&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	}
	elsif ($type eq 'data::node::repository::list')
	{
		$tfsClient->{'client'}->PUT("cards/$self->{'TrelloId'}?name=$name&idList=$parent->{'TrelloId'}&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	}
	else
	{
		$tfsClient->{'client'}->PUT("cards/$self->{'TrelloId'}?name=$name&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	}

	my $resp = $tfsClient->getResponse();
	$parent->getChild();
	$cardParent->getChild();
	return 0;

}

=head2 getCommentaire

Create a commentary repository with the type data::node::repository::card::commentaire

=cut


sub getCommentaire
{
	my $self = shift;
	my $node =data::node::repository::card::commentaire->new(0755, "Commentaire", $self->{'path'}, "$self->{id}/Commentaire");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);
}

=head2 getDescription

Create a desciption file with the type data::node::file::card::description

=cut


sub getDescription
{
	my $self = shift;
	my $node =data::node::file::card::description->new(0755, "Description", $self->{'path'}, "$self->{id}/Description");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);
}

=head2 getDateLimit

Create a date limit file with the type data::node::repository::card::dateLimit

=cut


sub getDateLimit
{
	my $self = shift;
	my $node =data::node::file::card::dateLimit->new(0755, "DateLimit", $self->{'path'}, "$self->{id}/DateLimit");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);
}

=head2 createChild

Create a new checklist in the trello card

=cut


sub createChild 
{
	my $self = shift;
	my $name = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{'client'}->POST("cards/$self->{TrelloId}/checklists?name=$name&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	$tfsClient->getResponse();
	$self->getChild();
	return 0;


}


sub remove
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	my $parent = tools::listUtils->getFile($self->{racine});
	if (ref $parent eq 'data::node::repository::board::archive::card')
	{
		$tfsClient->{client}->DELETE("cards/$self->{TrelloId}/?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	}
	else
	{
		$tfsClient->{client}->PUT("cards/$self->{TrelloId}/closed?value=true&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	}
	$tfsClient->getResponse();
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut



1;
