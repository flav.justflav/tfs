# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::organisation::user;
use parent "data::node::repository";
use strict;
use utf8;
use warnings;
use tools::listUtils;
use data::node::file::user;
use Data::Dumper qw/Dumper/;

=pod

=head1 NAME

data::node::repository::organisation::user

=head1 SYNOPSIS
	use data::node::repository::organisation::user
	
	data::node::repository::organisation::user->new(MODE, NAME, RACINE, ID);

=head1 DESCRIPTION

This package create a repository node, It override the getChild function, this function will return all user of an organization (team).

=head1 FUNCTIONS

=cut

=pod

=head2 new

This is the constructor which create the data::node::repository::oraganisation::user. 

=cut

sub new {
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = "$racine/$name";
	my $node = $class->SUPER::new($mode,$name,$racine,$id,$path);
	bless $node, $class;
	return $node;

}
=pod

=head2 getChild

This function add to he child list all the user of one trello organisation

=cut

sub getChild
{
	my $self = shift;
	my $tfsClient =	tools::tfsClient->getInstance();
	$self->{child} = [];
	my $parent = tools::listUtils->getFile($self->{racine});	
	$tfsClient->{client}->GET("organizations/$parent->{TrelloId}/members?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $users = $tfsClient->getResponse();
	foreach my $user (@{$users})
	{
		$user->{fullName} = tools::tfsClient->encodeText($user->{fullName});	
		my $node = data::node::file::user->new(0755,$user->{fullName},$self->{path},"orgaU/".$user->{id},"$self->{path}/$user->{fullName}",$user);
		$node->{TrelloId} = $user->{id};
		tools::listUtils->addNode($self,$node);
	}
	return $self->{child};	
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
