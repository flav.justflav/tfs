# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::list;

use parent "data::node::repository";
use Data::Dumper qw/Dumper/;
use strict;
use utf8;
use warnings;


=pod

=head1 NAME

data::node::repository::list

=head1 SYNOPSIS
	use data::node::repository::list

	data::node::repository::label->new(MODE, NAME, RACINE, ID);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository and call list. It override the getChild function who give all the card for one list.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::list like data::node::repository he take Unix mode, a name, a racine and an id. Return the object.


=cut


sub new
{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);

	bless $node, $class;
	return $node;
}


=pod

=head2 getChild

Return the childs of the node. this function get all the card who is contain in the list and create a data::node::repository::card object with each of them.

=cut


sub getChild
{
    my $self = shift;
    my $tfsClient =	tools::tfsClient->getInstance();

    $self->{child} =	[];

    $tfsClient->{client}->GET("lists/$self->{TrelloId}/cards?key=$tools::tfsClient::key&token=$tools::tfsClient::token");

    my $cards = $tfsClient->getResponse();
    foreach my $c (@{$cards})
    {
	$c->{name} = tools::tfsClient->encodeText($c->{name});	
        my $node = data::node::repository::card->new(0755, $c->{'name'}, $self->{'path'}, $c->{'id'}, "$self->{path}/$c->{name}");
	$node->{TrelloId} = $c->{'id'};
        tools::listUtils->addNode($self, $node);

    }
    return $self->{child};	


}

=head2 createChild

This function create a new list from trello board.

=cut

sub createChild
{
	my $self = shift;
	my $name = shift;

	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{client}->POST("cards?name=$name&idList=$self->{TrelloId}&keepFromSource=all&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	$tfsClient->getResponse();
	
	$self->getChild();
}

=head2 move

This function change the state of trello list to archive to unarchive. 

=cut

sub move
{
	my $self = shift;
	my $newPath = shift;
	my $path = "";
	if ($newPath =~ /(.+)\/.*/)
	{
		$path = $1;
	}
	my $parent = tools::listUtils->getFile($path);
	my $listParent = tools::listUtils->getFile($self->{racine});

	my $type = ref($parent);
	my $tfsClient = tools::tfsClient->getInstance();
	my $board = tools::listUtils->getFile($parent->{racine});
	if (ref $listParent eq 'data::node::repository::board::archive::list' && $type eq 'data::node::repository::board::byList')
	{
		$tfsClient->{'client'}->PUT("lists/$self->{'TrelloId'}?closed=false&idBoard=$board->{'TrelloId'}&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	}
	elsif ($type eq 'data::node::repository::board::byList')
	{
		$tfsClient->{'client'}->PUT("list/$self->{'TrelloId'}?idBoard=$board->{'TrelloId'}&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	}
	else
	{
		return -EINVAL();
	}

	my $resp = $tfsClient->getResponse();
	print Dumper $resp;
	$parent->getChild();
	$listParent->getChild();
	return 0;

}

=head2 remove

This function remove list from trello board.

=cut



sub remove
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();

	$tfsClient->{client}->PUT("lists/$self->{TrelloId}/closed?value=true&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	$tfsClient->getResponse();
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
