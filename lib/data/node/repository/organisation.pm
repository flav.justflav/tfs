# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::organisation;
use parent "data::node::repository";

use data::node::repository::organisation::user;
use data::node::repository::board;
use tools::listUtils;

=pod

=head1 NAME

data::node::repository::organisation

=head1 SYNOPSIS
	use data::node::repository::organisation

	data::node::repository::organisation->new(MODE, NAME, RACINE, ID);

MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository and call organisation. It override the getChild function.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::organisation like data::node::repository he take Unix mode, a name, a racine and an id and return the object.


=cut
sub new {
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	
	my $path = "$racine/$name";

	$path =~ s/^\///g;
	my $node = $class->SUPER::new($mode,$name,$racine,$id,$path);

	bless $node, $class;
	return $node;
}

=pod

=head2 getChild

Return the childs of the node. this function call the function getBoard and getUser

=cut

sub getChild
{
	my $self = shift;	
	my $tfsClient =	tools::tfsClient->getInstance();
	$self->{child} = [];
	
	$self->getBoard();
	$self->getUser();
	return $self->{child};
}

=pod

=head2 getBoard

This function add to the child all the board of one trello organisation.

=cut

sub getBoard
{
	my $self = shift;
	my $client = tools::tfsClient->getInstance();
	my $node;

	my $organisation = $client->getKey("organisation"); 
	$client->{'client'}->GET("organizations/$organisation/boards?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	
	my $boards = $client->getResponse();
	
	if($boards){
		foreach my $val (@{$boards}) {
			$val->{name} = tools::tfsClient->encodeText($val->{name});	
			$node = data::node::repository::board->new(0755, $val->{'name'}, $self->{'path'}, $val->{'id'}, $self->{'path'}.'/'.$val->{'name'});
			$node->{TrelloId} = $val->{id};
			tools::listUtils->addNode($self,$node);
		}
	}
	return $self->{'child'};
}

=pod

=head2 getUser

This function add to the child a data::node::repository::organisation::user object.
 
=cut
sub getUser
{
	my $self = shift;
	my $node = data::node::repository::organisation::user->new(0755,"Users",$self->{path}, "$self->{id}/Users");	
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);
}

=head2 createChild

This function create a new child. This child will be a new trello board.

=cut
sub createChild
{
	my $self = shift;
	my $name = shift;
	
	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{'client'}->POST("boards/?name=$name&idOrganization=$self->{TrelloId}&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	$tfsClient->getResponse();
	$self->getChild();
	return 0;


}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
