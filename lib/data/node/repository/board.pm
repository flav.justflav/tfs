# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::repository::board;
use parent "data::node::repository";
use data::node::repository::board::user;
use data::node::repository::board::byLabel;
use data::node::repository::board::byList;
use data::node::repository::board::byUser;
use data::node::repository::board::byDate;
use data::node::repository::board::cards;
use data::node::repository::board::archive;
use data::node::repository::board::customfields;
use data::list::customfields;
use tools::listUtils;
use Data::Dumper qw/Dumper/;

=pod

=head1 NAME

data::node::repository::board

=head1 SYNOPSIS
	use data::node::repository::board

	data::node::repository::board->new(MODE, NAME, RACINE, ID);

	data::node::repository::board->getChild();
MODE : Unix mode

=head1 DESCRIPTION

This package create a node with type repository and call board. It override the getChild function.

=head1 FUNCTIONS

=cut

=pod 

=head2 new

This is the constructor of data::node::repository::board like data::node::repository he take Unix mode, a name, a racine and an id and return the object.


=cut

sub new{

	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	
	my $path = "$racine/$name";
	
	my $node = $class->SUPER::new($mode, $name, $racine, $id, $path);
	
	bless $node, $class;
	return $node;
}

=pod 

=head2 getChild

Return the childs of the node. this function call the function getCards, getCardByList, getCardByList, getCardByUsern getArchive, getCardByDate and getUser.

=cut


sub getChild {
	
	my $self = shift;
	$self->{child} = [];	
	$self->getUser();
	$self->getCardByLabel();
	$self->getCards();
	$self->getCardByList();
	$self->getCardByUser();
	$self->getCardByDate();
	$self->getArchive();
	$self->getCustomfields();
	return $self->{child};
}

=pod

=head2 getUser

This function add to the child a data::node::repository::board::user object.
 
=cut

sub getCustomfields
{
    my $self = shift;

    my $list = data::list::customfields->getInstance();
    $list->addAllCustomfieldsbyboard($self->{id});

    my $node = data::node::repository::board::customfields->new(0755, "customfields", $self->{'path'}, "$self->{id}/customfields");
    $node->{TrelloId} = $self->{id};
    tools::listUtils->addNode($self,$node);
}

sub getUser{
	my $self = shift;
	
	my $node = data::node::repository::board::user->new(0755, "Users", $self->{'path'}, "$self->{id}/Users");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);
}

=pod

=head2 getCardByLabel

This function add to the child a data::node::repository::board::byLabel object.
 
=cut
sub getCardByLabel
{
	my $self = shift;
	
	my $node = data::node::repository::board::byLabel->new(0755, "CardByLabel", $self->{'path'}, "$self->{id}/cardByLabel");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);
}

=pod

=head2 getCards

This function add to the child a data::node::repository::board::cards object.
 
=cut
sub getCards
{
	my $self = shift;
	
	my $node = data::node::repository::board::cards->new(0755, "Cards", $self->{'path'}, "$self->{id}/cards");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self, $node);
	
}
=pod

=head2 getCardByList

This function add to the child a data::node::repository::board::byList object.
 
=cut
sub getCardByList
{
	my $self = shift;
	
	my $node = data::node::repository::board::byList->new(0755, "CardByList", $self->{'path'}, "$self->{id}/cardByList");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);

}

=pod

=head2 getCardByUser

This function add to the child a data::node::repository::board::byUser object.
 
=cut
sub getCardByUser
{
	my $self = shift;
	
	my $node = data::node::repository::board::byUser->new(0755, "CardByUser", $self->{'path'}, "$self->{id}/cardByUser");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);

}

=pod

=head2 getCardByDate

This function add to the child a data::node::repository::board::byDate object.
 
=cut

sub getCardByDate
{
	my $self = shift;
	
	print Dumper $self;
	my $node = data::node::repository::board::byDate->new(0755, "CardByDate", $self->{'path'}, "$self->{id}/Date");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);
}

=pod

=head2 getArchive

This function add to the child a data::node::repository::board::archive object.
This repository will contains all cards and list from the trello archive.
 
=cut

sub getArchive
{
	my $self = shift;
	
	my $node = data::node::repository::board::archive->new(0755, "Archive", $self->{'path'}, "$self->{id}/Archive");
	$node->{TrelloId} = $self->{id};
	tools::listUtils->addNode($self,$node);


}


=pod

=head2 remove

Delete all data board when called

=cut

sub remove
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{'client'}->DELETE("boards/$self->{TrelloId}?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $response = $tfsClient->getResponse();
	if ($response)
	{
		return 0;
	}
	return 1;
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


1;
