# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::file::card::info;
use parent "data::node::file";
use strict;
use warnings;

=head1 NAME

data::node::file::card::info node who represent trello card info with the file type

=head1 SYNOPSIS

	use data::node::file::card::info;
	
	data::node::file::card::info->new(unix mode, name, racine, id, path[, content]);
	

=head1 DESCRIPTION

This package create a node with type file and call info. This file represente a trello card info.
 
=head1 FUNCTIONS

=cut


=head2 new

This is the constructor of data::node::file::card::info like data::node::file he take Unix mode, a name, a racine and an id and return the object, with a optional content.


=cut


sub new {
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $content = shift;

	my $path = "$racine/$name";

	my $node = $class->SUPER::new($mode,$name,$racine,$id,$path,$content);
	bless $node, $class;
	return $node;
}

=head2 getContent

This function set the content with json of the trello object and return it.
 
=cut


sub getContent
{
	my $self = shift;
	my $tfsClient =	tools::tfsClient->getInstance();
	my $parent = tools::listUtils->getFile($self->{racine});
	$tfsClient->{client}->GET("cards/$parent->{id}?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $card = $tfsClient->getResponse();
	$self->{content} = JSON::XS->new->utf8->encode($card);
	return $self->{content};
}

1;
