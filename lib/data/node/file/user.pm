# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::file::user;
use parent "data::node::file";
use strict;
use warnings;
use JSON::XS;
use Data::Dumper qw/Dumper/;

=head1 NAME

data::node::file::user node who represent trello member with the file type

=head1 SYNOPSIS

	use data::node::file::user;
	
	data::node::file::user->new(unix mode, name, racine, id, path[, content]);
	
	data::node::file::user->remove();

=head1 DESCRIPTION

This package create a node with type file and call user. This file represente a trello user.
 
=head1 FUNCTIONS

=cut



=head2 new

This is the constructor of data::node::file::user like data::node::file he take Unix mode, a name, a racine and an id and return the object, with a optional content.


=cut


sub new {
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = shift;
	my $content = shift;


	my $node = $class->SUPER::new($mode,$name,$racine,$id,$path,$content);
	bless $node, $class;
	return $node;
}

=head2 getContent

This function set the content with json of the trello object and return it.
 
=cut

sub getContent
{
	my $self = shift;
	my $tfsClient =	tools::tfsClient->getInstance();
	
	my $id = $self->{TrelloId};
	$tfsClient->{client}->GET("members/$id?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $user = $tfsClient->getResponse();
	$self->{content} = JSON::XS->new->utf8->encode($user);
	return $self->{content};
}

=head2 move

This function remove the user from the first card and add it to the second. Before to move this function check if user exist in this board.

=cut

sub move
{
	my $self = shift;
	my $newPath = shift;
	my $path = "";
	my $name = "";
	if ($newPath =~ /(.+)\/(.*)/)
	{
		$path = $1;
		$name = $2;
	}

	my $parent = tools::listUtils->getFile($self->{'racine'});
	my $card = tools::listUtils->getFile($parent->{'racine'});
	my $view = tools::listUtils->getFile($card->{'racine'});
	my $board = tools::listUtils->getFile($view->{racine});
	my $btype = ref($board);
	if ($btype ne 'data::node::repository::board')
	{
		$board = tools::listUtils->getFile($board->{'racine'});
	}

	my $type = ref($card);
	if ($type eq 'data::node::repository::card')
	{
		my $tfsClient =	tools::tfsClient->getInstance();
		my $userList = data::list::user->getInstance();
		my $userId = "";
		if (($userId) = $userList->checkUserExist($name,$board->{id}))
		{
			my $oldId="";
			if ($self->{id} =~ /(.+)\/.+/)
			{
				$oldId = $1;
			}
			$tfsClient->{'client'}->DELETE("cards/$card->{id}/idMembers/$oldId?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
			$tfsClient->getResponse();
			$tfsClient->{'client'}->POST("cards/$card->{id}/idMembers?value=$userId&key=$tools::tfsClient::key&token=$tools::tfsClient::token");
			$tfsClient->getResponse();
			return 0;
		}
		else
		{
			return 1;
		}
	}
}

=head2 remove

This function remove a member from a card.

=cut

sub remove
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	my $parent = tools::listUtils->getFile($self->{racine});
	my $card = tools::listUtils->getFile($parent->{racine});
	if (ref($card) eq 'data::node::repository::card')
	{
		$tfsClient->{client}->DELETE("cards/$card->{TrelloId}/idMembers/$self->{TrelloId}?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
		$tfsClient->getResponse;
	}
	return 0;
}
1;
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


