# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node::file::label;
use parent "data::node::file";
use strict;
use warnings;

=head1 NAME

data::node::file::label node who represent trello label with the file type

=head1 SYNOPSIS

	use data::node::file::label;
	
	data::node::file::label->new(unix mode, name, racine, id, path[, content]);
	
	data::node::file::label->remove();

=head1 DESCRIPTION

This package create a node with type file and call label. This file represente a trello label.
 
=head1 FUNCTIONS

=cut



=head2 new

This is the constructor of data::node::file::label like data::node::file he take Unix mode, a name, a racine and an id and return the object, with a optional content.


=cut

sub new {
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = shift;
	my $content = shift;


	my $node = $class->SUPER::new($mode,$name,$racine,$id,$path,$content);
	bless $node, $class;
	return $node;
}

=head2 getContent

This function set the content with json of the trello object and return it.
 
=cut

sub getContent
{
	my $self = shift;
	my $tfsClient =	tools::tfsClient->getInstance();
	my $id = $self->{id};
	$id =~ s/(.+)\/.+/$1/;
	$tfsClient->{client}->GET("labels/$id?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	my $label = $tfsClient->getResponse();
	$self->{content} = JSON::XS->new->allow_nonref->utf8->encode($label);
	return $self->{content};
}

=head2 remove 

This function is call when you want to remove a label from a card, it does the trello call to delete it.

=cut 

sub remove
{
	my $self = shift;
	my $tfsClient = tools::tfsClient->getInstance();
	my $parent = tools::listUtils->getFile($self->{racine});
	my $card = tools::listUtils->getFile($parent->{racine});
	$tfsClient->{client}->DELETE("cards/$card->{TrelloId}/idLabels/$self->{TrelloId}?key=$tools::tfsClient::key&token=$tools::tfsClient::token");
	$tfsClient->getResponse;
	return 0;
}
1;
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


