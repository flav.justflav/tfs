# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::list::customfields;

use strict;
use warnings;
use tools::tfsClient;

sub _new
{
    my $class = shift;
    my $hash = {};
    bless $hash, $class;

    return $hash;
}

sub getInstance
{
    if (!$data::list::customfields::_instance) {
	$data::list::customfields::_instance = data::list::customfields->_new();
    }

    return $data::list::customfields::_instance;
}

sub getCustomfieldsName
{
    my $self = shift;
    my $boardId = shift;
    my $id = shift;

    return $self->{$boardId}->{$id}->name;
}

sub addAllCustomfieldsbyboard
{
    my $self = shift;
    my $boardId = shift;

    my $tfsClient = tools::tfsClient->getInstance();
    $tfsClient->{client}->GET("/boards/$boardId/?fields=name&customFields=true&key=$tools::tfsClient::key&token=$tools::tfsClient::token");

    my $h_customfields = $tfsClient->getResponse();
    foreach my $customfield (@{$h_customfields->{customFields}}) {
	$customfield->{name} = tools::tfsClient->encodeText($customfield->{name});
	$self->{$boardId}->{$customfield->{id}} = $customfield;
    }
    return undef;
}

1;
