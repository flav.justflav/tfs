# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::list::label;
=pod

=head1 NAME

data::list::label Keep list of label present in trello board.

=head1 SYNOPSIS

	use data::list::label;

	$list = data::list::label->getInstance();
	
	$list->checkLabelExist($labelName,$board->{TrelloId});

=head1 DESCRIPTION

This package keep trello board label and can use it in cache to avoid a suplementary call to the trello api.

=head1 FUNCTIONS

=cut


use strict;
use warnings;
use Data::Dumper qw/Dumper/;

=head2 _new

This function is the constructor don't use it, use getInstance.

=cut

sub _new {
	my $class = shift;
	my $hash = {};
	bless $hash, $class;
	return $hash;
}

=head2 getInstance

This function return the instance of the object. It this instance is not yet create, this function will create it before return.

=cut

sub getInstance {
	if (!$data::list::label::_instance)
	{
		$data::list::label::_instance = data::list::label->_new();
	}
	return $data::list::label::_instance;
}

=head2 addLabel

Add a label to the object.

=cut

sub addLabel {
	my $self = shift;
	my $label = shift;
	if ($label->{realName} eq "")
	{
		$label->{realName} = "void";
	}
	$self->{$label->{idBoard}}->{$label->{color}}->{$label->{realName}} = $label;
}

=head2 removeLabel

Not yet implement

=cut

sub removeLabel {
	my $self = shift;
	my $label = shift;
	my $board = shift;
}

=head2 checkLabelExist

This function will check if the label exist on the object if the label doesn't exist on cache, it will search all the label of the board. each label get from trello request will be add in the object for the next call of the function.

=cut

sub checkLabelExist
{
	my $self = shift;
	my $label_name = shift;
	my $boardId = shift;

	my $label_color = "";

	if ($label_name =~ /(.+)_(.*)/)
	{
		$label_color = $1;
		$label_name  = $2;
		if ($2 eq "")
		{
			$label_name = "void";
		}
	}
	return $self->{$boardId}->{$label_color}->{$label_name}->{id} if exists $self->{$boardId}->{$label_color}->{$label_name};

	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{client}->GET("boards/$boardId/labels?key=$tools::tfsClient::key&token=$tools::tfsClient::token");

	my $labels = $tfsClient->getResponse();
	foreach my $val (@{$labels}){
		my $realName = $val->{name};
		if ($realName eq "")
		{
			$realName = "void",
		}
		$val->{realName} = $realName;
		$self->addLabel($val);
		utf8::encode($val->{name});
		if ($label_name eq $val->{name} && $label_color eq $val->{color})
		{
			return $val->{id};
		}
		elsif ($label_name eq $val->{realName} && $label_color eq $val->{color})
		{
			return $val->{id};
		}
	}
	return undef;
}
1;
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut


