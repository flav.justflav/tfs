# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::list::user;

=pod

=head1 NAME

data::list::user Keep list of user present in trello board.

=head1 SYNOPSIS

	use data::list::user;

	$list = data::list::user->getInstance();
	
	$list->checkUserExist($userName,$board->{TrelloId});

=head1 DESCRIPTION

This package keep trello board user and can use it in cache to avoid a suplementary call to the trello api.

=head1 FUNCTIONS

=cut


use strict;
use warnings;
use Data::Dumper qw/Dumper/;

=head2 _new

This function is the constructor don't use it, use getInstance.

=cut


sub _new {
	my $class = shift;
	my $hash = {};
	bless $hash, $class;
	return $hash;
}

=head2 getInstance

This function return the instance of the object. It this instance is not yet create, this function will create it before return.

=cut


sub getInstance {
	if (!$data::list::user::_instance)
	{
		$data::list::user::_instance = data::list::user->_new();
	}
	return $data::list::user::_instance;
}

=head2 addUser

Add a user to the object.

=cut


sub addUser {
	my $self = shift;
	my $user = shift;
	my $boardId = shift;
	$self->{$boardId}->{$user->{fullName}} = $user;
}

=head2 checkUserExist

This function will check if the user exist on the object if the user doesn't exist on cache, it will search all the user of the board. each user get from trello request will be add in the object for the next call of the function.

=cut


sub checkUserExist
{
	my $self = shift;
	my $user_name = shift;
	my $boardId = shift;

	return $self->{$boardId}->{$user_name}->{id} if exists $self->{$boardId}->{$user_name};

	my $tfsClient = tools::tfsClient->getInstance();
	$tfsClient->{client}->GET("boards/$boardId/members?key=$tools::tfsClient::key&token=$tools::tfsClient::token");

	my $users = $tfsClient->getResponse();
	foreach my $val (@{$users}){
		utf8::encode($val->{fullName});
		$self->addUser($val, $boardId);
		if ($val->{fullName} eq $user_name)
		{
			return $val->{id};
		}
	}
	return undef;
}
1;

=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut
