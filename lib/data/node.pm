# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::node;

use strict;
use warnings;
use tools::tfsClient;
=pod

=head1 NAME

data::node 

=head1 SYNOPSIS

	use parent "data::node";

	sub {
		my $class = shift;
		...
		my $node = $class->SUPER::new(MODE,NAME,RACINE,ID,PATH);
		...
	}

MODE   : Unix MODE 

=head1 DESCRIPTION

This package create a node, this package is the parent of data::node::file and data::node::repository package. It give a set of accessor for the name, id ... .

=head1 FUNCTIONS

=cut


=pod

=head2 new

This is the constructor of the data::node package, he take a UNIX mode , a name , a racine , an id and a path in params and return the object.
IN : Unix mode, String name , String Racine , String Id, String Path
OUT : data::node

=head3 data::node object

	data::node :	
	{
		mode   => Unix mode,
		name   => String,
		id     => String,
		racine => String,
		path   => String,
		uid    => Uid number of current user,
		ctime  => Time of the node creation,
		mtime  => Time of the node creation,
		gid    => Array of gid

	}

=cut

sub new
{
	my $class = shift;
	my $mode = shift;
	my $name = shift;
	my $racine = shift;
	my $id = shift;
	my $path = shift;

	my $node = {mode => $mode, name => $name, racine => $racine, path => $path, id => $id};
	if ($node->{name} eq ".")
	{
		$node->{path} = $node->{racine};
		$node->{racine} = "/";
	}
	$node->{uid} = $<;
	$node->{ctime} = time();
	$node->{mtime} = time();
	my @tab = split(' ', $();
	$node->{gid} = \@tab;
	bless $node,$class;
	return $node;
}

=pod

=head2 getName

Get Accessor for the name attribute.

=cut

sub getName
{
	my $self = shift;
	return $self->{name};
}


=pod

=head2 getUid

Get Accessor for the uid attribute.

=cut

sub getUid
{
	my $self = shift;
	return $self->{uid};
}


=pod

=head2 getGid

Get Accessor for the gid attribute.

=cut

sub getGid
{
	my $self = shift;
	return $self->{gid};
}

=pod

=head2 getRacine

Get Accessor for the racine attribute.

=cut

sub getRacine
{
	my $self = shift;
	return $self->{racine};
}


=pod

=head2 getCtime

Get Accessor for the ctime attribute.

=cut

sub getCtime
{
	my $self = shift;
	return $self->{ctime};
}


=pod

=head2 getMtime

Get Accessor for the mtime attribute.

=cut

sub getMtime
{
	my $self = shift;
	return $self->{mtime};
}

=pod

=head2 setCtime

Set Accessor for the ctime attribute.

=cut

sub setCtime
{
	my $self = shift;
	$self->{ctime} = shift;
}


=pod

=head2 setMtime

Set Accessor for the mtime attribute.

=cut

sub setMtime
{
	my $self = shift;
	$self->{mtime} = shift;
}


=pod

=head2 getMode

Get Accessor for the mode attribute.

=cut

sub getMode
{
	my $self = shift;
	return $self->{mode};
}

=head2 move

Fonction call by the fuse main when user want to move a file or repository without move implementation.

=cut

sub move
{
	return 0;
}

=head2 remove

Fonction call by the fuse main when user want to remove a file or repository without remove implementation.

=cut


sub remove
{
	return 1;
}
=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut

1;
