# tfs - Trello File System
# Copyright (C) 2019 -- ITSGroup
# https://gitlab.com/flav.justflav/tfs
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License along
# with this program; if not, write to the Free Software Foundation, Inc.,
# 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

package data::list;

use strict;
use warnings;
use Data::Dumper qw/Dumper/;
=pod

=head1 NAME

data::list keep information from trello

=head1 SYNOPSIS

	use data::list;
	data::list->getInstance();


=head1 DESCRIPTION

This package keep data from trello api response. You can get him with the function getInstance. you can add data from trello with the function addData and you can get data of one trello object by using the function getNodeInfo.

=head1 FUNCTIONS

=cut



=pod

=head2 _new

This is a private constructor for the package don't use it. This function s already use by the function getInstance

=cut

sub _new {
	my $class = shift;
	my $hash = {};
	bless $hash, $class;
	return $hash;
}

=pod

=head2 getInstance

This function initialise the list and return it.

=cut


sub getInstance {
	if (!$data::list::_instance)
	{
		$data::list::_instance = data::list->_new();
	}
	return $data::list::_instance;
}

=pod

=head2 getNodeInfo

Return the object with the id pass in param.
in : ID
out : perl object or hash


=cut

sub getNodeInfo{
	my $self = shift;
	my $dataid = shift // "";
	if (exists $self->{$dataid})
	{
		return $self->{$dataid};
	}
	else 
	{
		print "Can't get file '$dataid'\n";
		return undef;
	}
}

=pod

=head2 addData

add hash pass in params to the list. the param need a id attribute. this attribute will be used for the list id.
in : perl object or hash
out : int
if out value is equals to 1, the data is add for the first time else the data is update

=cut

sub addData{
	my $self = shift;
	my $data = shift;
	my $i = 0;	
	if (!$self->{$data->{id}})
	{
		$i = 1;		
	}
	$self->{$data->{id}} = $data;
	return $i;
}

=pod

=head1 AUTHOR

Guillemot Alexandre <aguillemot@integra.fr>.

=cut

1;
